<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>Concerto Screen Content Audit</title>

		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="container" style="padding: 15px 0px;">
			<?php
				require_once('meekrodb.class.php');
				DB::$user = 'user';
				DB::$password = 'pass';
				DB::$dbName = 'concerto_production';
								
				$screens = DB::query('select id, name, location from screens order by location');
				
				foreach ($screens as $screen) {
					$ads = DB::query('select distinct feeds.parent_id, feeds.name as feed, contents.name as content, contents.end_time, contents.type from contents join submissions on submissions.content_id = contents.id join subscriptions on subscriptions.feed_id = submissions.feed_id join screens on screens.id = subscriptions.screen_id join templates on templates.id = screens.template_id join fields on fields.id = subscriptions.field_id join positions on positions.field_id = subscriptions.field_id and positions.template_id = screens.template_id join feeds on subscriptions.feed_id = feeds.id where screens.id = %i and submissions.moderation_flag = 1 and contents.parent_id is null and contents.end_time > current_date order by feeds.name, contents.end_time, contents.name', $screen['id']);
					
					echo '<div class="row"><div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					<div class="panel-heading">'.$screen['name'].' ('.$screen['location'].')</div>
					<table class="table table-striped table-hover table-condensed">
						<thead>
							<th style="width:33%">Feed</th>
							<th style="width:34%">Content</th>
							<th style="width:33%">End</th>
						</thead>
						<tbody>';
					
					foreach ($ads as $ad) {
						echo '<tr class="small">
							<td style="width:33%">';
						
						if ($ad['parent_id'] != "") {
							$parent = DB::queryOneField('name', 'select name from feeds where id = %i', $ad['parent_id']);
							echo $parent.' <span class="glyphicon glyphicon-chevron-right"></span> ';
						}
						echo $ad['feed'];
						echo '</td>
							<td style="width:34%">'.$ad['content'].' <span class="small">('.$ad['type'].')</span></td>
							<td style="width:33%">'.$ad['end_time'].'</td>
						</tr>';
					}
						
					echo '	</tbody>
					</table>
				</div>
			</div></div>';
					
				}
			?>
			
		</div>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	</body>
</html>